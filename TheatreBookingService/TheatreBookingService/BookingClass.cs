using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheatreBookingService
{
    public class BookingClass
    {
        private int bookingID;
        private int customerID;
        private int showingID;
        private double totalCost;

        
       // private string creditCardNumber;
        public int BookingID
        {
            get { return bookingID; }
            set { bookingID = value; }
        }
        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }
        public int ShowingID
        {
            get { return showingID; }
            set { showingID = value; }
        }
        public double TotalCost
        {
            get { return totalCost; }
            set { totalCost = value; }
        }
        
    }
}
