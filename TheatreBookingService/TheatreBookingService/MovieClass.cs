using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//for images 
using System.Drawing;

namespace TheatreBookingService
{
    public class MovieClass
    {
        private int _movieID;
        private string _title;
        private string _description;
        private Image _movieImage;

        public int MovieID
        {
            get { return _movieID; }
            set { _movieID = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public Image MovieImage
        {
            get { return _movieImage; }
            set { _movieImage = value; }
        }
    }
}
