using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TheatreBookingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IShowing" in both code and config file together.
    [ServiceContract]
    public interface IShowing
    {
        [OperationContract]
        void AddShowing(ShowingClass showing); //add showing of a movie

        [OperationContract]
        DataTable getShowingDetails();
    }
}
