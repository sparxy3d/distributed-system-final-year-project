using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheatreBookingClient
{
    public partial class AddShowing : Form
    {
        TheatreService.MovieClient client = null;

        public AddShowing()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                TheatreService.ShowingClass showing = new TheatreService.ShowingClass();
                TheatreService.ShowingClient client = new TheatreService.ShowingClient();


            showing.MovieID = Convert.ToInt32(cbMovies.Text);
            showing.ShowingDate = dateTimePicker1.Value;
            showing.ShowingTime = comboBox1.Text;
            showing.ShowingPrice = numericUpDown1.Value;
           // showing.show

            client.AddShowing(showing);
            

                if (cbMovies.Text == string.Empty)
                {
                    MessageBox.Show("Please select the Movie Titile", "About Title", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }


                else
                {
                    showing.MovieID = Convert.ToInt32(cbMovies.Text);
                }

                showing.ShowingDate = dateTimePicker1.Value;

                if (comboBox1.Text == string.Empty)
                {
                    MessageBox.Show("Please select a Showing Time", "About Time", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }

                else
                {
                    showing.ShowingTime = comboBox1.Text;
                }

                client.AddShowing(showing);
            }

            catch (Exception ex)
            {
                MessageBox.Show("An Error in Process, Please Check again" + ex.Message, "Error Message", MessageBoxButtons.OK,
                                   MessageBoxIcon.Exclamation);
            }

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Value < DateTime.Today.Date)
            {
                MessageBox.Show("Date should be a Future value", "About Date", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
        }

        private void AddShowing_Load(object sender, EventArgs e)
        {
            //********************
            //load on comboBox
            try
            {
                client = new TheatreService.MovieClient();
                //code for label and comboBox
                //call in form load this method
                //    bookClass = new BookingClass();
                this.cbMovies.DisplayMember = "movie_id";//column that you want to show on combo Box
                this.cbMovies.ValueMember = "movie_id";

                this.cbMovies.DataSource = client.getMovieDetails();

            }
            catch (CommunicationException fx)
            {
                MessageBox.Show("Communication error - Please make sure that the host is connected and open the form:  \r\n\r\n" + fx.Message, "cannot connect to the host computer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SendKeys.Send("%{F4}");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception has occured:\r\n " + ex.Message, "Exception error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //*******************
        }
    }
}
