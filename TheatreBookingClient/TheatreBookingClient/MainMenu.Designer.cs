﻿namespace TheatreBookingClient
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.moviesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addMovieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addShowingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayMoviesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ticketSalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayTicketSalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewCustomerRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moviesToolStripMenuItem,
            this.ticketSalesToolStripMenuItem,
            this.customerToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(740, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // moviesToolStripMenuItem
            // 
            this.moviesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMovieToolStripMenuItem,
            this.addShowingToolStripMenuItem,
            this.displayMoviesToolStripMenuItem});
            this.moviesToolStripMenuItem.Name = "moviesToolStripMenuItem";
            this.moviesToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.moviesToolStripMenuItem.Text = "Movies";
            // 
            // addMovieToolStripMenuItem
            // 
            this.addMovieToolStripMenuItem.Name = "addMovieToolStripMenuItem";
            this.addMovieToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.addMovieToolStripMenuItem.Text = "Add Movie";
            this.addMovieToolStripMenuItem.Click += new System.EventHandler(this.addMovieToolStripMenuItem_Click);
            // 
            // addShowingToolStripMenuItem
            // 
            this.addShowingToolStripMenuItem.Name = "addShowingToolStripMenuItem";
            this.addShowingToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.addShowingToolStripMenuItem.Text = "Add Showing";
            this.addShowingToolStripMenuItem.Click += new System.EventHandler(this.addShowingToolStripMenuItem_Click);
            // 
            // displayMoviesToolStripMenuItem
            // 
            this.displayMoviesToolStripMenuItem.Name = "displayMoviesToolStripMenuItem";
            this.displayMoviesToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.displayMoviesToolStripMenuItem.Text = "Display Movies";
            this.displayMoviesToolStripMenuItem.Click += new System.EventHandler(this.displayMoviesToolStripMenuItem_Click);
            // 
            // ticketSalesToolStripMenuItem
            // 
            this.ticketSalesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayTicketSalesToolStripMenuItem});
            this.ticketSalesToolStripMenuItem.Name = "ticketSalesToolStripMenuItem";
            this.ticketSalesToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.ticketSalesToolStripMenuItem.Text = "Ticket Sales";
            // 
            // displayTicketSalesToolStripMenuItem
            // 
            this.displayTicketSalesToolStripMenuItem.Name = "displayTicketSalesToolStripMenuItem";
            this.displayTicketSalesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.displayTicketSalesToolStripMenuItem.Text = "Display Ticket Sales";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewCustomerRecordsToolStripMenuItem});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.customerToolStripMenuItem.Text = "Customer";
            // 
            // viewCustomerRecordsToolStripMenuItem
            // 
            this.viewCustomerRecordsToolStripMenuItem.Name = "viewCustomerRecordsToolStripMenuItem";
            this.viewCustomerRecordsToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.viewCustomerRecordsToolStripMenuItem.Text = "View Customer Records";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 363);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainMenu";
            this.Text = "Main Menu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem moviesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addMovieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addShowingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ticketSalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayTicketSalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewCustomerRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayMoviesToolStripMenuItem;
    }
}