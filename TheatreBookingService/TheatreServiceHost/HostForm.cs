using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.ServiceModel;

namespace TheatreServiceHost
{
    public partial class HostForm : Form
    {
        ServiceHost host;

        public HostForm()
        {
            InitializeComponent();
            lblStatus.Text = "Host closed";
            //maybe start service automatically here
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            host = new ServiceHost(typeof(TheatreBookingService.TheatreService));
            host.Open();
            lblStatus.Text = "Host started on " + DateTime.Now.ToString();
            btnStart.Enabled = false;
            btnStop.Enabled = true;
        }


        private void btnStop_Click(object sender, EventArgs e)
        {
            host.Close();
            lblStatus.Text = "Host closed";
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }
    
    }
}
