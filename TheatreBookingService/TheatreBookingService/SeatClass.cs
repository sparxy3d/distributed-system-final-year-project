using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheatreBookingService
{
    public class SeatClass
    {
        private char seatRow;
        private int seatNumber;
        private int showingID; //from showingClass
        private int available;
        private int bookingID;

        public int seatCount; //no need encapsulation for this
        public SeatClass[] seatObject = new SeatClass[200]; //each seat is an object in this array


        public char SeatRow
        {
            get { return seatRow; }
            set { seatRow = value; }
        }
        public int SeatNumber
        {
            get { return seatNumber; }
            set { seatNumber = value; }
        }
        public int ShowingID
        {
            get { return showingID; }
            set { showingID = value; }
        }
        public int Available
        {
            get { return available; }
            set { available = value; }
        }
        public int BookingID
        {
            get { return bookingID; }
            set { bookingID = value; }
        }
    }
}
