﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheatreBookingClient
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void addMovieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddMovie frmAddMovie = new AddMovie();
            frmAddMovie.ShowDialog();
        }

        private void addShowingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddShowing frmAddPerformance = new AddShowing();
            frmAddPerformance.ShowDialog();
        }

        private void displayMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisplayMovies frmDisplayMovies = new DisplayMovies();
            frmDisplayMovies.ShowDialog();
        }
    }
}
