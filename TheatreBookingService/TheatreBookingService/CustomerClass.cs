using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheatreBookingService
{
    public class CustomerClass
    {
        private int customerID;
        private string title;
        private string firstName;
        private string lastName;
        private DateTime dateOfBirth;
        private string nicNumber;
        private double mobile;
        private string email;
        private string username;
        private string password;

        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }
        public string NicNumber
        {
            get { return nicNumber; }
            set { nicNumber = value; }
        }
        public double Mobile
        {
            get { return mobile; }
            set { mobile = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
    }
}
