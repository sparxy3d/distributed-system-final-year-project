using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TheatreBookingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMovie" in both code and config file together.
    [ServiceContract]
    public interface IMovie
    {
        [OperationContract]
        void AddMovie(MovieClass movie);

        [OperationContract]
        DataTable getMovieDetails();
    }
}
