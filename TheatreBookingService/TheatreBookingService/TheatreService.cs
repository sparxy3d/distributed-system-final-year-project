using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TheatreBookingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TheatreService" in both code and config file together.
    public class TheatreService : IMovie, IShowing, ICustomer, ISeatService
    {

        SeatClass sc = null;

        //Add a movie to the database
        public void AddMovie(MovieClass movie)
        {
            DatabaseConnector databaseConnector = new DatabaseConnector();

            databaseConnector.sqlConnection.Open();

            databaseConnector.startSqlCommand("INSERT INTO movie(title, description) VALUES(@ttl,@desc)");
            databaseConnector.sqlCommand.Parameters.AddWithValue("@ttl", movie.Title);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@desc", movie.Description);
            //databaseConnector.sqlCommand.Parameters.AddWithValue("@img", movie.MovieImage);

            databaseConnector.sqlCommand.ExecuteNonQuery();

            databaseConnector.sqlConnection.Close();
        }

        public void AddShowing(ShowingClass showing)
        {
            //First save the showing in the database
            //Then get the automatically generated showing ID for the seats form
            DatabaseConnector databaseConnector = new DatabaseConnector();

            databaseConnector.sqlConnection.Open();

            databaseConnector.startSqlCommand("INSERT INTO showing(movie_id, showing_date, showing_time, ticket_price) VALUES(@mid, @sd, @st, @tp)");
           // databaseConnector.sqlCommand.Parameters.AddWithValue("@sid", showing.ShowingID); showing id is automatic
            databaseConnector.sqlCommand.Parameters.AddWithValue("@mid",showing.MovieID);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@sd", showing.ShowingDate);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@st", showing.ShowingTime);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@tp", showing.ShowingPrice);


            databaseConnector.sqlCommand.ExecuteNonQuery();

            //now get the showing id that is automatically generated and pass it to assign seat method,
            //so that it can be used to generate seats per performance in the database.

            databaseConnector.startSqlCommand("SELECT MAX(showing_id) FROM showing");//select the latest added showing and create seats
            showing.ShowingID = Convert.ToInt32(databaseConnector.sqlCommand.ExecuteScalar());  //int showingID or use object
            showing.assignSeats(showing.ShowingID);
            databaseConnector.sqlConnection.Close();
            
        }

        public int SaveCustomer(CustomerClass customer)
        {
            DatabaseConnector databaseConnector = new DatabaseConnector();

            databaseConnector.sqlConnection.Open();

            databaseConnector.startSqlCommand("INSERT INTO customer(title, first_name, last_name, dob, nic, mobile, email, username, password) VALUES(@tt, @fn,@ln,@dob,@nic,@mob,@em,@un, @psw)");
            databaseConnector.sqlCommand.Parameters.AddWithValue("@tt", customer.Title);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@fn", customer.FirstName);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@ln", customer.LastName);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@dob", customer.DateOfBirth);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@nic", customer.NicNumber);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@mob", customer.Mobile);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@em", customer.Email);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@un", customer.Username);
            databaseConnector.sqlCommand.Parameters.AddWithValue("@psw", customer.Password);


            databaseConnector.sqlCommand.ExecuteNonQuery();
            databaseConnector.startSqlCommand("SELECT MAX(cid) FROM customer"); //select customer id from customer db
            customer.CustomerID = Convert.ToInt32(databaseConnector.sqlCommand.ExecuteScalar());
            databaseConnector.sqlConnection.Close();

            return customer.CustomerID;

            
        }


        public System.Data.DataTable getShowingDetails()
        {
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.sqlConnection.Open();

            var showingDataTable = databaseConnector.getData("SELECT showing_id from showing WHERE showing_date >= GETDATE()");

            //WHERE showing date > = today date , if showing date is before today seats cannot be booked for that movie
            databaseConnector.sqlConnection.Close();
            return showingDataTable;

        }


        public System.Data.DataTable getMovieDetails()
        {
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.sqlConnection.Open();

            var movieDataTable = databaseConnector.getData("SELECT movie_id FROM movie");

            databaseConnector.sqlConnection.Close();
            return movieDataTable;
        }

        public SeatClass loadSeats(int showingID)
        {
            sc = new SeatClass();
            sc.seatCount = 0;
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.sqlConnection.Open();
            databaseConnector.getData("SELECT * FROM seat WHERE showing_id = "+showingID+ "");
            sc.seatCount = databaseConnector.mainDataSet.Tables[0].Rows.Count;

            for (int i = 0; i < sc.seatCount; i++ )
            {//load values from database for each seat
                sc.seatObject[i] = new SeatClass();
                databaseConnector.mainDataRow = databaseConnector.mainDataSet.Tables[0].Rows[i];
                sc.seatObject[i].SeatRow = Convert.ToChar(databaseConnector.mainDataRow[0]);
                sc.seatObject[i].SeatNumber = (int)(databaseConnector.mainDataRow[1]);
                sc.seatObject[i].ShowingID = (int)(databaseConnector.mainDataRow[2]);
                sc.seatObject[i].Available = (int)(databaseConnector.mainDataRow[3]);

            }

            return sc;
                //******************
               

            //*****************
        }


        public void updateSeat(int showingID, char seatRow, int seatNumber)
        {//to update seat table and make booked seats as available=1
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.sqlConnection.Open();
            databaseConnector.startSqlCommand("UPDATE seat SET available='1' WHERE showing_id='" + showingID + "' AND seat_row='" + seatRow + "' AND seat_number='" + seatNumber + "'").ExecuteNonQuery();
            databaseConnector.sqlConnection.Close();
      
        }
    }
}
