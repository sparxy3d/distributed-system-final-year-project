﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheatreBookingClient
{
    public partial class AddMovie : Form
    {
        string imageName;
        public AddMovie()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                FileDialog fileDialog = new OpenFileDialog();
                fileDialog.Filter = "Image File(*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif";

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    imageName = fileDialog.FileName;
                    Bitmap newImg = new Bitmap(imageName);
                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox1.Image = (Image)newImg;
                }
                fileDialog = null;
            }

            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                TheatreService.MovieClass movie = new TheatreService.MovieClass(); //movie class reference
                TheatreService.MovieClient client = new TheatreService.MovieClient(); //theatre service client reference


                //--validation
                if (txtTitle.Text == string.Empty)
                {
                    MessageBox.Show("Please enter the Movie Titile", "About Title", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }

                else
                {
                    movie.Title = txtTitle.Text;
                }

                if (txtDescription.Text == string.Empty)
                {
                    MessageBox.Show("Please enter the Movie Description", "About Description", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }

                else
                {
                    movie.Description = txtDescription.Text;
                }

                client.AddMovie(movie);
            }

           

            catch (Exception ex)
            {
                MessageBox.Show("An Error in Process, Please Check again" + ex.Message, "Error Message", MessageBoxButtons.OK,
                                   MessageBoxIcon.Exclamation);
            }
        }
    }
}
