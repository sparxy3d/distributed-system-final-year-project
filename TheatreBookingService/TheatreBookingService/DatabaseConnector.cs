using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace TheatreBookingService
{
    public class DatabaseConnector
    {
        public SqlConnection sqlConnection;

        public DataSet mainDataSet; //assign as public for seats


        private string connectionString = ConfigurationManager.ConnectionStrings["theatre_db"].ConnectionString;

        public SqlCommand sqlCommand;

        public DataRow mainDataRow;
        
     
        //Constructor
        public DatabaseConnector()
        {
            // connectionString = ConfigurationManager.ConnectionStrings["theatre_db_connection_string"].ConnectionString;
            //connect to database and assign to sqlConnection object
            sqlConnection = new SqlConnection(connectionString);
        }

        public SqlCommand startSqlCommand(string query)
        {
            sqlCommand = new SqlCommand(query, sqlConnection);
            return sqlCommand;
        }


        //to get from the database
        public DataTable getData(string query)
        {
            var dataTable = new DataTable();
            var dataSet = new DataSet();
            var dataAdapter = new SqlDataAdapter { SelectCommand = startSqlCommand(query) };
            // var dataView = new DataView();

            dataAdapter.Fill(dataSet);
            mainDataSet = dataSet;
            dataTable = dataSet.Tables[0];      //all tables assignted as table[0]
            return dataTable;
        }



    }
}
