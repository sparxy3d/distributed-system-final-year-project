using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheatreBookingService
{
    public class ShowingClass
    {
        //this class contains showings for each movie

        private int showingID;
        private int movieID; //from Movie class
        private DateTime showingDate;
        private string showingTime;
        private decimal showingPrice;

        
        public int ShowingID
        {
            get { return showingID; }
            set { showingID = value; }
        }
        public int MovieID
        {
            get { return movieID; }
            set { movieID = value; }
        }
        public DateTime ShowingDate
        {
            get { return showingDate; }
            set { showingDate = value; }
        }
        public string ShowingTime
        {
            get { return showingTime; }
            set { showingTime = value; }
        }

        public decimal ShowingPrice
        {
            get { return showingPrice; }
            set { showingPrice = value; }
        }

        public void assignSeats(int showingID)
        {
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.sqlConnection.Open();
            //for each seat call sqlcommand method
            for (int r=1; r<=12; r++)
            {
                char rowLetter = Convert.ToChar(64+r); //start from character A
                if (r > 8)   //according to other cinema seatmap, I is not used so use this if loop
                    rowLetter = Convert.ToChar(64 + r + 1);
                for(int s=1; s <= 15; s++)
                {
                    databaseConnector.startSqlCommand("INSERT INTO seat(seat_row, seat_number,showing_id) VALUES(@rl,@sn,@sid)");
                    databaseConnector.sqlCommand.Parameters.AddWithValue("@rl", rowLetter);
                    databaseConnector.sqlCommand.Parameters.AddWithValue("@sn", s);
                    databaseConnector.sqlCommand.Parameters.AddWithValue("@sid", showingID); //passed from addshowing method

                    databaseConnector.sqlCommand.ExecuteNonQuery();
                }
            }
            databaseConnector.sqlConnection.Close(); //if have time create trigger to delete seats created on date after showing
        }
    }
}
