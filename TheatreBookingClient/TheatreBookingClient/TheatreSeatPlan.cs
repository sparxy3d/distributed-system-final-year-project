using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheatreBookingClient
{
    public partial class TheatreSeatPlan : Form
    {
        Button[,] btnSeat = new Button[30, 20];//this is to set buttons as seat
        Label[] label = new Label[12];// aisle = to separate seat map

        int[,] seatStatus = new int[30, 20]; //keep track of status of seat


        //create object of showing to load into combobox
        TheatreService.ShowingClient client = null;
        TheatreService.ShowingClass showingClass = null;

        TheatreService.SeatServiceClient seatClient = null;
        TheatreService.SeatClass seatClass = null;

        public TheatreSeatPlan()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


      
        

        private void TheatreSeatPlan_Load(object sender, EventArgs e)
        {
            //load on comboBox
            try
            {
                client = new TheatreService.ShowingClient();
                //code for label and comboBox
                //call in form load this method
                //    bookClass = new BookingClass();
                this.cbShowing.DisplayMember = "showing_id";//column that you want to show on combo Box
                this.cbShowing.ValueMember = "showing_id";

                this.cbShowing.DataSource = client.getShowingDetails();

            }
            catch (CommunicationException fx)
            {
                MessageBox.Show("Communication error - Please make sure that the host is connected and open the form:  \r\n\r\n" + fx.Message, "cannot connect to the host computer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SendKeys.Send("%{F4}");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception has occured:\r\n " + ex.Message, "Exception error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        //**************************
        private void drawSeatPlan()  
        {//copy and paste same thing to ticket check form 
            try
            {
                int offset; //this is for the difference
                int maxSeats;
                for (int j = 1; j <= 11; j++)//j  is rows , here 7 rows
                {
                    maxSeats = 20;
                    //if (j == 2) maxSeats = 15;
                    if (j == 1) maxSeats = 14;  //1rd row
                    if (j == 2) maxSeats = 16;  //2th row
                    if (j == 3) maxSeats = 17;  //3th row
                    if (j == 4) maxSeats = 19;  //4th row
                    if (j >= 7 && j <=9) maxSeats = 19;  //7th row-9th
                    if (j == 11) maxSeats = 19;

                    for (int i = 1; i < maxSeats; i++) // i is for columns
                    {
                        offset = 0;
                        if (j == 1) offset = 4; //from left, offset is to remove the seats
                        if (j == 2) offset = 3;
                        if (j == 3) offset = 2;
                        if (j == 4) offset = 1;
                        if (j >= 7 && j<= 9) offset = 1;


                        btnSeat[i, j] = new Button();  //creating a new button for each seat
                        btnSeat[i, j].Width = 28; //set width and height of each button
                        btnSeat[i, j].Height = 28;

                        //from left the distance from each console, offset is variable so it increases/decrease for each row
                        btnSeat[i, j].Left = ((28 * i) + (28 * offset));
                        btnSeat[i, j].Top = (28 * j);

                        if ((i + offset) > 15) btnSeat[i, j].Left += 28;

                        //if i and offset is greater than 17 or 5 then move by 28 pixels to put character map eg A,B,C...
                      //  if (i + offset > 17) btnSeat[i, j].Left += 28;
                      //  if (i + offset > 5) btnSeat[i, j].Left += 28;


                      //  btnSeat[i, j].Top = (30 * j); //to adjust one row below the other

                        btnSeat[i, j].Image = TheatreBookingClient.Properties.Resources.seat1;

                        if (seatStatus[i, j] == 1) 
                            btnSeat[i, j].Image = TheatreBookingClient.Properties.Resources.seat2;

                        //============this is code for making slected buttons yellow 1, give each button a name here
                        string buttonName = "btn";
                        if (j <= 9) buttonName += " "; //adding a space eg btn 1 for loop in i
                        buttonName += j;


                        if (i <= 9) buttonName += " "; //adding a space eg btn 1 1, 1 2 , 1 3 for loop in i
                        buttonName += i;
                        btnSeat[i, j].Name = buttonName;
                        btnSeat[i, j].Click += new EventHandler(seat_Click);// see seat click

                        pnlSeatMap.Controls.Add(btnSeat[i, j]);

                        //----------tooltip for each button / seat
                        int rowNumber = j;
                        if (j > 8) rowNumber++;

                        //using ascii to get tooltip text eg j=1,  64+j = 65=A ; A1
                        string tooltipText = Convert.ToChar(rowNumber + 64).ToString() + (i).ToString();
                        ToolTip buttonToolTip = new ToolTip();
                        buttonToolTip.SetToolTip(btnSeat[i, j], tooltipText);

                        /*      if (seatStatus[i, j] == 1)
                                  btnSeat[i, j].Image = AirlineApplication.Properties.Resources.seat2;

                              


                              //====================================================
                              pnlPlane.Controls.Add(btnSeat[i, j]);

                              

                        */

                          }//end of i loop


                          //first label A B C D E .....
                          //setting properties for the label
                          label[j] = new Label();
                          label[j].Size = new System.Drawing.Size(24, 30);
                          char c = Convert.ToChar(64 + j);  //same ascii principle as above 65 ---> A
                            
                            if (j > 8)
                                c=Convert.ToChar (65 + j);
                            
                          label[j].Text = Convert.ToString(c);
                          label[j].Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                          //for each j it prints the variable here we set the location x variable is constant while y value changes according to j
                          label[j].Location = new System.Drawing.Point(455, 4 + 28 * j);
                          pnlSeatMap.Controls.Add(label[j]);

                /*          ///second label A B C D E..........
                          lblClass2[j] = new Label();
                          lblClass2[j].Size = new System.Drawing.Size(24, 30);
                          char d = Convert.ToChar(64 + j);  //for alphabet

                          lblClass2[j].Text = Convert.ToString(d);
                          lblClass2[j].Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                          lblClass2[j].Location = new System.Drawing.Point(575, 10 + 30 * j);
                          pnlPlane.Controls.Add(lblClass2[j]);
                              
                    */
                }//end j loop
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error! Please restart the application and make sure all images are present \r\nException Error : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void seat_Click(object sender, EventArgs e)
        {
            //**********************
            //create button from above method 
            try
            {

                Button clickedButton = (Button)sender;

                //breaking down the button to obtain row and seat number
                string s = clickedButton.Name;
                int j = Convert.ToInt16(s.Substring(3, 2)); //example btn 1 5 - after use substring, 1 will return
                int i = Convert.ToInt16(s.Substring(5, 2));//example btn 1 5 - after use substring, 5 will return

                if (seatStatus[i, j] != 1) //if 1 means its booked so we check if not booked
                {
                    if (seatStatus[i, j] == 0) //if not booked and is empty make it yellow
                    {

                        seatStatus[i, j] = 2;   //assign 2 as in available
                        btnSeat[i, j].Image = TheatreBookingClient.Properties.Resources.seat3;
                    }
                    else //if its 2 and clicked make it back to red
                    {
                        seatStatus[i, j] = 0; //if seat is already selected and you click again then make it red
                        btnSeat[i, j].Image = TheatreBookingClient.Properties.Resources.seat1;
                    }
                     listSelectedSeats(); //whenever clicked it calls this method to add seats to listbox, and even when remove seats it can remove from listbox
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {//if input string ever becomes less than 5 characters catch this exception
                MessageBox.Show("Error! \r\nException Error : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex1)
            {
                MessageBox.Show("Error!  \r\nException Error : " + ex1.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cbShowing_SelectedIndexChanged(object sender, EventArgs e)
        {
            //*******************************
            
                //take flight id from comboBox selection
                int showingID = int.Parse(cbShowing.Text);
                seatClient = new TheatreService.SeatServiceClient();
                seatClass = new TheatreService.SeatClass();
                // Load the seats for the selected flights
                seatClass = seatClient.loadSeats(showingID);
                for (int i = 0; i < seatClass.seatCount; i++)
                {//gettinig seatrow, seatnum and available
                    char c = seatClass.seatObject[i].SeatRow;
                    int r = (int)c - 64;        //subtract 64 and change back to integer,eg A = 65-64= 1
                    int s = seatClass.seatObject[i].SeatNumber;
                    int available = seatClass.seatObject[i].Available;
                    seatStatus[s, r] = available; //integer available assigned to array we declare top 
                }

                int totalButtons = pnlSeatMap.Controls.Count; //now to remove the extra buttons that we have offset or removed from above
                for (int i = 0; i < totalButtons; i++)
                {//existing seatbuttons removed from panel when selectedindex change
                    pnlSeatMap.Controls.RemoveAt(0);
                }


                drawSeatPlan();//call the draw plan method for each flight changed
           

            
        }

        private void listSelectedSeats()
        {// this method to take selected seats to listboxd
            //CHECK THIS
            //double totalFare = 0;
            try
            {

                listBox1.Items.Clear();
                for (int j = 1; j <= 11; j++)
                {
                    for (int i = 1; i <= 20; i++)
                    {
                        if (seatStatus[i, j] == 2)//if seat is selected
                        {
                            char c = Convert.ToChar(64 + j);
                            listBox1.Items.Add("Row " + c + " Seat " + i);
                          //  totalFare += Convert.ToInt32(label3.Text);//adding the fare for each seat

                        }
                    }
                }
              //  label1.Text = totalFare.ToString("f2");

            }

            catch (Exception ex1)
            {
                MessageBox.Show("Error seat not selected " + ex1.Message, "Error ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {//when btnClear is pressed

            try
            {
                //first clear listbox
                listBox1.Items.Clear();
                //then clear selected items and make it yellow
                for (int j = 1; j <= 11; j++)
                {
                    for (int i = 1; i <= 20; i++)
                    {
                        if (seatStatus[i, j] == 2) //when cleared whatever is  yellow make it back to seat 1 image
                        {
                            seatStatus[i, j] = 0;
                            btnSeat[i, j].Image = TheatreBookingClient.Properties.Resources.seat1;

                        }
                    }
                }
                // totalFare = 0;
                label3.Text = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            //CHECK THIS
            //**************************
            try
            {//Validation
                if (listBox1.Items.Count > 0)//Customer must at least make one booking
                {//seatLimit is static variable from seatClass
                    if (listBox1.Items.Count <= 6) //Booking limit per customer is 6 


                    //try to use variable
                    {
                        //pass the details from this bookseatplan form to booking form
                       
                        int showingID = int.Parse(cbShowing.Text);

                        //then save all to booking table
                        //assigning the form detalis into class variable to store in database

                        char seatRow;
                        int seatNum;
                        int rowNumber;
                        //update seatRecords in database
                        seatClient = new TheatreService.SeatServiceClient();

                        //  seatClass seatObj = new seatClass();
                        seatClass = seatClient.loadSeats(Convert.ToInt32(showingID));

                        for (int i = 0; i < seatClass.seatCount; i++)
                        {
                            //get row and number from seat class and assign here
                            seatRow = seatClass.seatObject[i].SeatRow;
                            seatNum = seatClass.seatObject[i].SeatNumber;

                            rowNumber = (int)seatRow - 64; //take seatrow above and subtract from 64 coz we added before to get ascii here we subtract

                            if (seatStatus[seatNum, rowNumber] == 2) //seatStat is just the array variable assigned top
                            {//send to update seat class, which turns seat==2 into seat ==1 i.e.booked
                                seatClient.updateSeat(showingID, seatRow, seatNum);
                            }

                        }

                        MessageBox.Show("Data added successfully! Booking Completed", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show("Customer can only book a maximum of 6 seats.\r\nPlease make sure you select only 6 seats and try again", "Max booking limit exceeded", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnClear_Click(sender, e); //call the button1click event and clear all the listbox and panel selections
                    }
                }
                else
                {
                    MessageBox.Show("Customer should book at least one seat!", "Book at least one seat", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (CommunicationException fx)
            {
                MessageBox.Show("Communication error - Please make sure that the host is connected:  \r\n\r\n" + fx.Message, "cannot connect to the host computer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SendKeys.Send("%{F4}");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex, "Exception Error");
            }
        }

    }
}
